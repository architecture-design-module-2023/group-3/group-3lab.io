
# Videojuego online **World of Webcraft**


# Autores
- José Camacho
- Edison López
- Alejandro Sandoval

# Descripción

Este es la solución para un videojuego online tipo first-person shooter MMORPG.

Las actividades que podrán realizar los jugadores son las siguientes:
- Iniciar sesión - Crear nueva cuenta
- Seleccionar mapa, el cual desea jugar, luego se enfrentan con jugadores seleccionados al azar automáticamente con la misma habilidad.
- Jugadores podrán invitar a torneos, solo por invitación.
- Invitados podrán observar partidas pero sin ser vistos o intervenir.
- Jugadores pueden crear nuevos mapas, armas y reglas.

Los administradores tienen las siguientes actividades.
- Administrar jugadores registrados
- Anular baneos a jugadores, notificados por solicitud 

# Decisiones de arquitectura


La documentación de decisiones de arquitectura o Architecture Decision Records (ADR) es una colección de documentos que recogen individualmente cada una de las decisiones de arquitectura tomadas. 


|  A continuación definimos los siguientes: |
|---|
| [Granja de Servidores](adr/adr-granja-servidores.md)|
| [Latencia de Servidores](adr/adr-Latencia.md) |
| [Artistas Visuales](adr/adr-artistas-visuales.md) |


# Diagramas
Los diagramas estarán plasmados por el [modelo c4](https://c4model.com/)
Estos diagramas proporcionan diferentes niveles de abstracción, cada uno de los cuales es relevante para una audiencia diferente.

|  A continuación los de esta solución: |

- [Diagrama de Solución](diagramas/diagrama-solucion.dsl)
- [Galería de Imágenes Solución](diagramas/Imagenes%20C4/)