# Titulo
Baja latencia en la ejecucion del juego

## Estado
Aprobado

## Contexto

Los juegos MMORPG  requieren una latencia mínima debido a que los jugadores realizan disparos de forma coordinada y a una alta velocidad.Los diferentes valores de latencia entre jugadores pueden determinar el resultado de una partida puesto qe esto estará en función del tiempo de respuesta que tenga respecto al servidor de la partida


## Decisiones

Para asegurar  una baja latencia se sigue el modelo de ejecución de **cliente servidor** y será ejecutado en un navegaodr Web que estará implementado en PHP.Adicionalmente para la ejecución correcta de del juego en el navegador se intalará un plugin para el correcto funcionamiento del juego.

El juego estará basado en el sistema de ordenes por turno ejecutando una orden que precise una ventana temporal para que tenga validez durante los eventos en el juego.

Se opta tambien ubicar los servidores en lugares alrededor del mundo donde geográficamente Amazón tenga ubicados. Esta ténica de servidores conectados entre si es conocida como **Cluster o pool of server** y entre mas se tenga cerca el servidor con el cliente su latencia será mas baja, asegurando así que se pueda tener un ping menos a 60 ms

## Consecuencias

- Al aumenta la cantidad de servidores a utilizar
- aumenta el costo
- Mejora la latencia
- Afectará en la rapidez que se ejecute una acción

## Fecha Última Actualización
     12-Julio-2023