# Titulo
Artistas visuales para el diseño

## Estado
Aprobado

## Contexto
El diseño visual es un elemento crucial para destacar en el competitivo mercado de los juegos FPS (First Person Shooter). Con el objetivo de asegurar una calidad gráfica destacada, se ha tomado la decisión de contratar especialistas en el área de diseño gráfico enfocado en juegos.

## Decisiones
- Se contratará un equipo de **artistas visuales altamente calificados** para trabajar en el diseño gráfico del juego. Estos artistas se encargarán de crear modelos 3D, texturas, efectos visuales y todo el arte gráfico necesario para proporcionar una experiencia visual impactante y de alta calidad.
- Uso de software especializado en diseño gráfico: Para facilitar el trabajo del equipo de artistas visuales, se utilizarán herramientas y **software especializados** en diseño gráfico y producción de arte digital. Algunas opciones a considerar son:
        - **Adobe Photoshop**: Para la creación y edición de texturas y efectos visuales.
        - **Blender**: Para la creación y animación de modelos 3D.
        - **Substance Painter**: Para la texturización de modelos 3D.
        - **Unity**: Plataformas de desarrollo de juegos que ofrecen herramientas y sistemas de renderizado avanzados.
- Para lograr una calidad visual requerida, se utilizarán **tecnologías gráficas avanzadas**. Algunas decisiones técnicas a considerar son:
        - Uso de técnicas de iluminación global (Global Illumination) para crear ambientes realistas y bien iluminados.
        - Implementación de sombras dinámicas para mejorar la calidad visual y la sensación de profundidad.
        - Empleo de efectos visuales de partículas para agregar realismo y dramatismo a las escenas.
        - Utilización de técnicas de posprocesamiento, como efectos de desenfoque (bloom), corrección de color y tonemapping, para mejorar la calidad visual y crear atmósferas inmersivas.
## Consecuencias
- La contratación de artistas visuales profesionales permitirá crear un **diseño gráfico de mayor nivel**, lo que se traducirá en una experiencia visual más inmersiva y atractiva para los jugadores.
-  El uso de modelos 3D detallados, texturas de alta resolución y efectos visuales sofisticados puede aumentar los **requerimientos de rendimiento del juego** y la necesidad de equipos cliente con capacidades gráficas avanzadas.
- La contratación de un equipo de artistas visuales profesionales implica un **costo adicional** en términos de salarios y honorarios, así como la posible adquisición de herramientas y software especializados.
## Fecha Última Actualización
12-Julio-2023