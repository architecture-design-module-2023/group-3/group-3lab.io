# Titulo
Granja de Servidores con alto performance - Servidores de Zonas

## Estado
Aprobado

## Contexto

Para un juego en linea, en el cual tendrá millones de usuarios conectados al mismo tiempo interactuando, es necesario contar con una **granja de servidores con alto rendimiento (mayor CPU y RAM) en diferentes zonas del mundo** que permitan soportar una alta demanda de usuarios y no tenga problemas de latencia.

## Decisiones

Decidimos las instancias de servidores tipo C que ofrece AWS, las cuales son un segmento que funcionan bien con las cargas de trabajo de procesamiento por lotes, transcodificación de archivos multimedia, servidores web de alto rendimiento, servidores de videojuegos, entre otras aplicaciones con uso intentiso de computación.

Este segmento de servidores tienes las siguientes características:
- Procesadores AWS Graviton3 - procesadores basados en ARM
- Memorias DDR5: Ofrecen un 50% mas de ancho de banda comparado con las DDR4
- Optimización para EBS
- Tecnología de AWS Nitro System, combinacion de hardware dedicado e hipervisor ligero.

>Para los servidores de zona, recomendamos la familia c7g que tiene una mejor relación precio-rendimiento y el tamaño de instancia **c7g.metal**, el mismo contiene lo siguiente:
>- 64 CPU Virtual
>- Memoria 128 Gb
>- Ancho de banda de red 30 Gbps
>- Ancho de banda de EBS 20 Gbps

>Adicional, en ciertas regiones como Sao Paulo o Londres no tienen las instancias de familia c7, por lo que sugerimos el tamaño de instancia **c6i.16xlarge**, el cual contiene lo siguiente:
>- 64 CPU Virtual
>- Memoria 128 Gb
>- Ancho de banda de red 25 Gbps
>- Ancho de banda de EBS 20 Gbps

> El precio estimado por mes se calcula de la siguiente forma: (Precio por región * horas al mes) + (Precio por hora * numero instancias * horas al mes).
>- El precio por región es fijo: **730 horas x 2 USD = 1460.0000 USD.**
>- El precio por instancia: **1 instancia x precio_hora X 730 horas.**

>A continuación, se visualiza el precio por hora de las instancias sugeridas en base a las regiones que ofrece AWS, a los totales se tiene que sumar el precio por región que es un valor fijo:
>| Región    | Latencias promedio (ms)|C7g.metal ($) | C6i.16xlarge ($) | Total mes C7g.metal ($) | Total mes C6i.16xlarge ($)
>| -------- | ------- |------- | ------- | ------- | ------- |
>| UsEast (N Virginia)  | 309    | 2.32 |2.992 | 1693.6 | 2184.16 | 
>| UsEast (Ohio) | 330     | 2.3123 | 2.992| 1687.979 | 2184.16 |
>| Europe(London)    | 551    | - | 3.5552| - | 2595.296 |
>| South America(Sao Paulo)    | 611    | - | 4.6112| - | 3366.176|

> El costo estimado por mes de un servidor con instancia dedicada y dependiendo del tipo de instancia está entre ***$3000 y $5000***.

>El equema de escalamiento para cada servidor de zona, recomendamos sea tipo horizontal.

## Consecuencias

- Al aumentar memoria y nucleos CPU, aumenta el costo por hora.
- Para el escalamiento horizontal aplicado en los servidores implica un menor costo pero puede resultar complejo el controlar el balanceo de carga de varios servidores en paralelo.

## Fecha Última Actualización
10-Julio-2023