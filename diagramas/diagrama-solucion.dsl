workspace "World of Webcarft" "First-person shooter MMORPG game" {

    model {
        playerOnline = person "Jugador Online" "Jugador Online de cualquier parte del mundo"
        playerAnonymous = person "Jugador Anónimo" "Jugador no registrado / modo espectador"
        administrator = person "Administrador" "Administrador de la plataforma"
        
        softwareSystem = softwareSystem "World of Webcraft" "First Person Shooter MMORPG game"{
            userManager = container "User manager" "Controla la lógica para definir que un usuario es jugador, administrador, jugador anónimo" "NodeJs - Express"
            webApplication = container "Web Application" "Servidor Web/Proxy, redirige a un sitio web estático para jugadores u administrador" "Nginx/PM2"
            webSiteAdmin = container "Sitio Web (Administrador)" "Sitio web donde se inicia sesión el administrador de la plataforma" "JavaScript, React" "Web Browser"
            dataBasePlayers = container "Base de datos" "Base de datos relacional, con información de los jugadores como historial, puntaje, skins, estados de cuenta (activo o bloqueado)" "Postgresql" "Database"
            webSitePlayers = container "Sitio Web (Jugador Online)" "Sitio web donde jugadores inician sesión, consultar puntaje, skins, etc" "JavaScript, React" "Web Browser"
            webSiteSession = container "Sitio web" "Sesión de juego iniciada" "Unity" "Web Browser" 
            dataBaseSession = container "Base de datos Sesion" "Base de datos no relacional, información relacionada a las sesiones de juego" "Amazon Dynamo DB" "Database"
            //1. Component - Jugador - Api Application
            apiApplicationPlayers = container "Api Application (Jugadores online)" "Conjunto de endpoints para consulta de información (Jugadores Online)" {
                layerRoutePlayer = component "Capa rutas" "Rutas de los servicios web publicados para su consumo" "Nodejs - Express"
                layerControllerPlayer = component "Capa controladores" "Capa de controladores que contiene la lógica de los servicios web" "Nodejs - Express"
                layerServicesPGSQLPlayer = component "Capa Servicios (Posgresql)" "Capa de servicios que contiene las consultas a realizar en la base de datos" "Nodejs - Express"
                layerServicesAmazon = component "Capa Servicios (Amazon Dynamo DB)" "Capa de servicios que contiene las consultas a realizar en la base de datos" "Nodejs - Express"
                
                webSitePlayers -> layerRoutePlayer "Usa" "JSON/HTTPS"
                layerRoutePlayer -> layerControllerPlayer "Usa"
                layerControllerPlayer -> layerServicesPGSQLPlayer "Usa"
                layerControllerPlayer -> layerServicesAmazon "Usa"
                layerServicesPGSQLPlayer -> dataBasePlayers "Lectura y Escritura de datos" "JDBC"
                layerServicesAmazon -> dataBaseSession "Lectura y Escritura de datos" "JDBC"
            }
            //2. Component - Admin - Api Application
            apiApplicationAdmin = container "Api Application (Administrador)" "Conjunto de endpoints para consulta de información (Administrador)" "NodeJS" {
                layerRouteAdmin = component "Capa rutas" "Rutas de los servicios web publicados para su consumo" "Nodejs - Express"
                layerControllerAdmin = component "Capa controladores" "Capa de controladores que contiene la lógica de los servicios web" "Nodejs - Express"
                layerServicesPGSQLAdmin = component "Capa Servicios (Posgresql)" "Capa de controladores que contiene la lógica de los servicios web" "Nodejs - Express"
                
                webSiteAdmin -> layerRouteAdmin "Usa" "JSON/HTTPS"
                layerRouteAdmin -> layerControllerAdmin "Usa"
                layerControllerAdmin -> layerServicesPGSQLAdmin "Usa"
                layerServicesPGSQLAdmin -> dataBasePlayers "Lectura y Escritura de datos" "JDBC"
            }
            //3. Component-Sesion Juego
            apiApplicationSession = container "Api Application (Sesión de Juego)" "Conjunto de servicios para la lectura y escritura de los datos relacionado a las sesiones de juego activas" "NodeJs - Express"{
                layerServerSocket = component "Capa Server Socket" "Capa de instancia de server socket" "Nodejs - Express - WebSockets"
                layerControllerSession = component "Capa controladores" "Capa de controladores que contiene la lógica de los servicios web" "Nodejs - Express"
                layerServicesAmazonSession = component "Capa Servicios (Amazon Dynamo DB)" "Capa de servicios que contiene las consultas a realizar en la base de datos" "Nodejs - Express"
                
                webSiteSession -> layerServerSocket "Usa" "JSON/HTTPS"
                layerServerSocket -> layerControllerSession "Usa"
                layerControllerSession -> layerServicesAmazonSession "Usa"
                layerServicesAmazonSession -> dataBaseSession "Lectura y Escritura de sesión de juego" "SQL/TCP"
            }
            //4.
            verifyZone = container "Verifcador de zona" "Verifica el servidor de zona mas cercano y disponibilidad de la sala" "Microservicio" "Microservicio"{
                layerGateway = component "Capa Gateway" "Capa de ejecución API Gateway" "Nodejs - Express"
                gameLiftSense = component "Game lift sense" "Capa-Game lift sense administra la flota de servidores disponibles" "Nodejs - Express"
            
                webSitePlayers -> layerGateway "Verifica el servidor de zona mas cercano y disponibilidad de la sala" "WebSockets"
                layerGateway -> gameLiftSense "Usa"
                gameLiftSense -> webSiteSession "Lectura y escritura de la sesión de juego" "WebSockets" 
                
            }
        }
  
        authSystem = softwareSystem "Sistema de autorización" "Sistema de autorización" {
            tags ExternalSystem
        }
        mailServices = softwareSystem "Servicio de mail" "Servicio de mail" {
            tags ExternalSystem
        }
        //Contexto
        playerOnline -> authSystem "Solicita acceso de player" "Person" "Person"
        administrator -> authSystem "Solicita acceso de admin" "Person"
        playerAnonymous -> authSystem "Sesión de Juego (modo espectador)"
        softwareSystem -> authSystem "Registro jugadores nuevos"
        
        softwareSystem -> mailServices "Envio de mail usando"
        mailServices -> playerOnline "Envia mail a"
        mailServices -> administrator "Envia mail a"
        //Cotendedores
        userManager -> webApplication "Usuario autorizado"
        webApplication -> webSiteAdmin "Redirige a sitio web (Administrador)"
        webSiteAdmin -> apiApplicationAdmin "Llamada Apis" "JSON/HTTPS"
        apiApplicationAdmin -> dataBasePlayers "Consulta Información" "SQL/TCP"
        //playerAnonymous -> userManager "Jugador Anónimo (Modo Espectador)"
        //playerOnline -> userManager "Inicia sesión"
        authSystem -> userManager "Auteticación"
        
        webApplication -> webSitePlayers "Redirige a sitio web (Jugador online)"
        webSitePlayers -> apiApplicationPlayers "Llamada Apis" "JSON/HTTPS"
        apiApplicationPlayers -> dataBaseSession "Consulta Información" "SQL/TCP"
        apiApplicationPlayers -> dataBasePlayers "Consulta Información" "SQL/TCP"
        webSitePlayers -> verifyZone "Jugador Selecciona iniciar Multijugador" "WebRTC"
        verifyZone -> webSiteSession "Inicia la sesión de juego" "WebRTC"
        webSiteSession -> apiApplicationSession "Lectura y escritura de la sesión de juego" "WebSockets"
        apiApplicationSession -> dataBaseSession "Lectura y escritura de la sesión de juego" "SQL/TCP"
        userManager -> webSiteSession "Redirige a sesión de juego - Jugador Anónimo (modo espectador)"
    }
    
    views { 
       
       styles {
            element "ExternalSystem" {
                background #808080
                color #ffffff
            }
            element "Web Browser" {
                shape WebBrowser
                background #438dd5
                color #ffffff
            }
            element "Database" {
                shape Cylinder
                background #438dd5
                color #ffffff
            }
            element "Microservicio" {
                shape Hexagon
                background #438dd5
                color #ffffff
            }
             element "Software System" {
                background #1168bd
                color #ffffff
            }
            element "Person" {
                shape person
                background #08427b
                color #ffffff
            }
            
        }
    }
    
   
}